### Авторизация приложения в Google Sheets API
- авторизоваться под своей учеткой EXMO в google
- включить "Google Sheets Api" - https://developers.google.com/sheets/api/quickstart/python и скачать credentials.json
- скачать credentials.json и забросить в текущую папку
- запустить $ python3 authorize.py (откроется вкладка браузера по предоставлению доступа к использованию Google Sheets Api через вашу учетку google, если все ok, то получите "The authentication flow has completed. You may close this window." и в текущей папке появится token.pickle)

### Подготовка и запуск утилиты
Пример таблиц с переводом:
- https://docs.google.com/spreadsheets/d/1QM5_3ZxPJ6rJXqxhaZVh5CwLVpoKXYqQspg6PkYXoys/edit#gid=0
- https://docs.google.com/spreadsheets/d/1lFuDwgGPhVm_bOVGlxgjMqCqM6hMGDWW5jWSVQm1XM4/edit#gid=0

## Установка зависимостей и запуск напрямую без docker'а 
- установить зависимости из `$ pip3 install -r requirements.json` (использую обычно `virtualenv` в качестве виртуального окружения, но docker лучше для этого подходит)
- $EXMO_LOCALES_DIR=<path_to_exmo_front_locales_dir> python3 exmotr.py --id=1QM5_3ZxPJ6rJXqxhaZVh5CwLVpoKXYqQspg6PkYXoys --key=verification.notes.take_photo

## Билд и запуск через docker (пользовался этим способом)
- собрать билд черезе `$ make build` из текущей папки
- удалить ключ в словаре: `docker run -it --rm -v $EXMO_LOCALES_DIR:/locales -v `pwd`:/code exmotr --key=verification.notes.take_photo --delete`
![удаление ключа](doc_images/delete_key.png)
- простой перевод: `docker run -it --rm -v $EXMO_LOCALES_DIR:/locales -v `pwd`:/code exmotr --id=1QM5_3ZxPJ6rJXqxhaZVh5CwLVpoKXYqQspg6PkYXoys --key=verification.notes.take_photo`
![перевод ключа](doc_images/translate_key.png)
- перевод с заменой переменной: `docker run -it --rm -v $EXMO_LOCALES_DIR:/locales -v `pwd`:/code exmotr --id=1lFuDwgGPhVm_bOVGlxgjMqCqM6hMGDWW5jWSVQm1XM4 --key=trade_orders_on_main.order_contains_count --replace-var=number --row=4`
![перевод ключа](doc_images/translate_key_var.png)

