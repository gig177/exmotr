FROM python:3.8.3-alpine

COPY ./requirements.txt ./credentials.json ./token.pickle ./exmotr.py /code/
RUN pip install -r /code/requirements.txt

ENV EXMO_LOCALES_DIR=/locales

WORKDIR /code

ENTRYPOINT ["python" , "/code/exmotr.py"]
