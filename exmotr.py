from __future__ import print_function
import pickle
#import os.path
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import argparse
import json
import re

from collections import OrderedDict

parser = argparse.ArgumentParser(description='Copy GoogleDocs translations into $EMO_ROOT/static/locales/*')
parser.add_argument('--id', dest='spreadsheetId', help='google docs id')
parser.add_argument('--key', dest='key', help='key of json file', required=True)
parser.add_argument('--row',  dest='row',  help='mapped row from google sheet to key of json file')
#parser.add_argument('--locales', dest='localesDir', help='path to locales directory')
parser.add_argument('--transform-func', dest='transformFunc', help='lambda function that transform field value')
parser.add_argument('--replace-var', dest='replaceVar', help='name of found var patter: {{some_var}}')
parser.add_argument('--lang', dest='lang', default='all', help='one or more lang to use (for example: --lang=en; or --lang=en,ru')
parser.add_argument('--ignore-lang', dest='ignoreLang', default='', help='ignore some lang (for example: --ignore-lang=zh')
parser.add_argument('--delete', dest='isDelete', action='store_true', default=False, help='should the --key be deleted from all json files')

def getSheetService():
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

    # The ID and range of a sample spreadsheet.
    #SAMPLE_SPREADSHEET_ID = '1b6PDvVoZ-5r-9v6_YU1cEE9mumv-gb1GK9wBBF6FB1k' # old table translates
    SAMPLE_SPREADSHEET_ID = '14Z5MxsruBZGC21MBXPW_bFKFBL8LhnEPz6TsftCet_Q'

    #SAMPLE_SPREADSHEET_ID = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms'
    SAMPLE_RANGE_NAME = 'Class Data!A5:E'

    #def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    return sheet

def readSheetRange(range=''):
    sheet = getSheetService()
    return sheet.values().get(spreadsheetId=args.spreadsheetId, range=range).execute().get('values', [])

def matchLangsInLocalesDir(usedLangs):
    langsInLocalesDir = os.listdir(localesDir)
    count = 0
    print('Matching langs in $EXMO_LOCALES_DIR...')
    for lang in usedLangs:
        if lang in langsInLocalesDir:
            count += 1
    if count != len(usedLangs):
        raise Exception('Some of langs was not found in $EXMO_LOCALES_DIR. Check out the environment variable path')

def matchLangsInGoogleDoc(usedLangs):
    values = readSheetRange(range='A1:L1')[0]
    count = 0
    print('Matching langs in google doc...')
    for field in values:
        try:
            isMatched = langsMap[field.lower()]
            print('lang col [%s] => matched' % field)
            count += 1
        except e:
            print(e)
    if count != len(langsMap):
        raise Exception('Some of langs was not matched')

def copyOrUpdate(data, value=None):
    isNew = args.key not in data
    if value:
        data.update({ args.key: value })
    if isNew and value:
        data.move_to_end(args.key, last=False)
    return (data, True)

def dropKey(data):
    isWrite = False
    if args.key in data:
        del data[args.key]
        isWrite = True
    return data, isWrite 


def transformValue(originValue):
    out = originValue
    if args.replaceVar:
        out = re.sub('\{.+?\}', '{{%s}}' % args.replaceVar, out)

    return eval('lambda %s' % args.transformFunc)(out) if args.transformFunc else out

def doTranslates(usedLangs):
    #matchLangs()

    selectRange = 'A%s:L%s' % (args.row, args.row)
    row = readSheetRange(range=selectRange)[0]

    columns = readSheetRange(range='A1:L1')[0]
    print()
    for i in range(len(columns)):
        lang = langsMap[ columns[i].strip().lower() ]
        if lang in usedLangs:

            value = transformValue(row[i].strip())
            doFileActionForLang(lang, copyOrUpdate, value=value)
            print('[%s] => %s' % (lang, value))


def doDropKey():

    for lang in langsMap.values():
        doFileActionForLang(lang, dropKey)


def doFileActionForLang(lang, actionFn, **kwargs):

    data = None
    with open(localesDir + lang + '/common.json', 'r') as translates:
        data = json.load(translates, object_pairs_hook=OrderedDict)
        """
        for pair in list(data.items())[0: 20]:
            print(pair)
        """

    out, isModified = actionFn(data, **kwargs)
    if not isModified: return
    
    """
    del data['confirm_phone_popup.title']
    data.update({ 'confirm_phone_popup.title': 'hello title' })
    data.update({ 'confirm_phone_popup.hello': 'lol' })
    data.move_to_end('confirm_phone_popup.hello', last=False)
    print('-----after------')
    for pair in list(data.items())[0: 10]:
        print(pair)
    print('last row', list(data.items())[-1])
    """
    with open(localesDir + lang + '/common.json', 'w') as translates:
        json.dump(out, translates, indent=4, ensure_ascii=False)
        translates.write("\n")

def detectUsedLangs():
    langs = langsMap.values() if args.lang == 'all' else [v.strip() for v in args.lang.split(',')]
    langsToIgnore = [v.strip() for v in args.ignoreLang.split(',')]
    return list( filter(lambda v: v not in langsToIgnore, langs) )

args = parser.parse_args()
localesDir = os.environ['EXMO_LOCALES_DIR'] + '/'
langsMap = OrderedDict({
    'en': 'en',
    'ru': 'ru',
    'uk': 'uk',
    'espanol': 'es',
    'french': 'fr',
    'deutsch': 'de',
    'italian': 'it',
    'portugues': 'pt',
    'romanian': 'ro',
    'chinese': 'zh',
    'polski': 'pl',
    'turkish': 'tr',
})

def createOrReadGoogleDocsCredentials():
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
    flow = InstalledAppFlow.from_client_secrets_file(
        'credentials.json', SCOPES)


def readGoogleAuthCredentials():
    if not os.path.isfile('credentials.json'):
        raise Exception('File credentials.json does not exist. You propaply should enable Google Sheets API and download credentials.json into docker build directory.')

    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

    creds = None

    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    print('.creds', creds)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds

def main():
    #creds = readGoogleAuthCredentials()

    usedLangs = detectUsedLangs()
    print('used langs:', usedLangs)

    matchLangsInLocalesDir(usedLangs)

    if args.isDelete and args.key and args.row is None and args.spreadsheetId is None:
        print('Deleting... key %s' % args.key)
        return doDropKey()

    if args.key and args.row and args.spreadsheetId:
        print('Translating...')
        doTranslates(usedLangs)


if __name__ == '__main__':
    main()
